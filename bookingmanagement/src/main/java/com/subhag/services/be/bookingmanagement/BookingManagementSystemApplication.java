package com.subhag.services.be.bookingmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan("com.subhag.services.be.commons.models")
@ComponentScan({ "com.subhag.services.be.bookingmanagement", "com.subhag.services.be.commons.*" })
public class BookingManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookingManagementSystemApplication.class, args);
	}
}
