package com.subhag.services.be.bookingmanagement.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import com.subhag.services.be.commons.models.Mail;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private org.thymeleaf.spring5.SpringTemplateEngine templateEngine;


    public void sendSimpleMessage(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {
       
        
        
        MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        helper.addAttachment("logo.png", new ClassPathResource("/templates/subhag-logo.png"));

        Context context = new Context();
        context.setVariables(mail.getModel());
        String html = templateEngine.process("email-booking-template", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());


        emailSender.send(message);
    }
    
 public void sendBookingConfirmation(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {      
        
        
        MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        helper.addAttachment("logo.png", new ClassPathResource("/templates/subhag-logo.png"));

        Context context = new Context();
        context.setVariables(mail.getModel());
        String html = templateEngine.process("email-booking-confirmation-template", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());


        emailSender.send(message);
    }
 
 
 public void sendBookingConfirmationToCustomer(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {      
     
     
     MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
     MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
             MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
             StandardCharsets.UTF_8.name());

     Context context = new Context();
     context.setVariables(mail.getModel());
     String html = templateEngine.process("email-customer-booking-template", context);

     helper.setTo(mail.getTo());
     helper.setText(html, true);
     helper.setSubject(mail.getSubject());
     helper.setFrom(mail.getFrom());


     emailSender.send(message);
 }

 
public void sendBookingConfirmationToAdminManager(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {      
     
     
     MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
     MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
             MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
             StandardCharsets.UTF_8.name());

     Context context = new Context();
     context.setVariables(mail.getModel());
     String html = templateEngine.process("email-booking-admin-manager-template", context);

     helper.setTo(mail.getTo());
     helper.setText(html, true);
     helper.setSubject(mail.getSubject());
     helper.setFrom(mail.getFrom());


     emailSender.send(message);
 }

public void sendBookingAppointmentToCustomer(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {      
    
    
    MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
            MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
            StandardCharsets.UTF_8.name());

    Context context = new Context();
    context.setVariables(mail.getModel());
    String html = templateEngine.process("email-customer-booking-appointment-template", context);

    helper.setTo(mail.getTo());
    helper.setText(html, true);
    helper.setSubject(mail.getSubject());
    helper.setFrom(mail.getFrom());


    emailSender.send(message);
}


public void sendBookingAppointmentToGYNC(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {      
    
    
    MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
            MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
            StandardCharsets.UTF_8.name());

    Context context = new Context();
    context.setVariables(mail.getModel());
    String html = templateEngine.process("email-gync-booking-scheduled-template", context);

    helper.setTo(mail.getTo());
    helper.setText(html, true);
    helper.setSubject(mail.getSubject());
    helper.setFrom(mail.getFrom());


    emailSender.send(message);
}


public void sendBookingCompleteToCustomer(Mail mail) throws MessagingException, IOException, javax.mail.MessagingException {      
    
    
    MimeMessage message = (MimeMessage) emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper((javax.mail.internet.MimeMessage) message,
            MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
            StandardCharsets.UTF_8.name());

    Context context = new Context();
    context.setVariables(mail.getModel());
    String html = templateEngine.process("email-customer-booking-complete-template", context);

    helper.setTo(mail.getTo());
    helper.setText(html, true);
    helper.setSubject(mail.getSubject());
    helper.setFrom(mail.getFrom());


    emailSender.send(message);
}

}
