package com.subhag.services.be.bookingmanagement.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.subhag.services.be.bookingmanagement.dto.BookingAssignedDto;
import com.subhag.services.be.bookingmanagement.dto.BookingAssociatedUserAnswerDto;
import com.subhag.services.be.bookingmanagement.dto.BookingCommentDto;
import com.subhag.services.be.bookingmanagement.dto.BookingCompleteDto;
import com.subhag.services.be.bookingmanagement.dto.BookingDto;
import com.subhag.services.be.bookingmanagement.dto.BookingOngoingDto;
import com.subhag.services.be.bookingmanagement.dto.BookingProviderDto;
import com.subhag.services.be.bookingmanagement.dto.BookingScheduledDto;
import com.subhag.services.be.bookingmanagement.dto.BookingUserFeedbackDto;
import com.subhag.services.be.bookingmanagement.dto.ServiceIdDTO;
import com.subhag.services.be.bookingmanagement.dto.UpdateBookingDto;
import com.subhag.services.be.bookingmanagement.service.BookingService;
import com.subhag.services.be.commons.models.Booking;
import com.subhag.services.be.commons.models.BookingAssociatedUserAnswer;
import com.subhag.services.be.commons.models.BookingComment;
import com.subhag.services.be.commons.models.UserBookingFeedback;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value="/api/v1/booking")
public class BookingController {
	
	@Autowired
	private BookingService bookingService;
	
	@ApiOperation(value = "Register a booking")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
	public Booking bookingNew(@RequestBody BookingDto bookingDto) throws Exception {
		return bookingService.bookingNew(bookingDto);
	}
	
	@ApiOperation(value = "Assign Providers for the booking")
	@RequestMapping(value = "/assignproviders", method = RequestMethod.PUT, produces = "application/json")
	public Booking bookingAssigned(@RequestBody BookingAssignedDto bookingAssignedDto) throws Exception {
		return bookingService.bookingAssigned(bookingAssignedDto);
	}
	
	@ApiOperation(value = "Set appointment for the booking")
	@RequestMapping(value = "/bookingscheduled", method = RequestMethod.PUT, produces = "application/json")
	public Booking bookingScheduled(@RequestBody BookingScheduledDto bookingScheduledDto) throws Exception {
		return bookingService.bookingScheduled(bookingScheduledDto);
	}
	
	@ApiOperation(value = "Consulatation started for the booking")
	@RequestMapping(value = "/bookingongoing", method = RequestMethod.PUT, produces = "application/json")
	public Booking bookingOngoing(@RequestBody BookingOngoingDto bookingOngoingDto) throws Exception {
		return bookingService.bookingOngoing(bookingOngoingDto);
	}
	
	@ApiOperation(value = "Consulatation complete for the booking")
	@RequestMapping(value = "/bookingcomplete", method = RequestMethod.PUT, produces = "application/json")
	public Booking bookingComplete(@RequestBody BookingCompleteDto bookingCompleteDto) throws Exception {
		return bookingService.bookingComplete(bookingCompleteDto);
	}
	
	@ApiOperation(value = "Get all available Services")
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
	public List<Booking> getAllBooking() throws Exception {
		return bookingService.getAllBooking();
	}
	
	@ApiOperation(value = "Get booking by bookingId")
	@RequestMapping(value = "/getbookingbybookingid", method = RequestMethod.POST, produces = "application/json")
	public Booking getBookingByBookingId(@RequestBody BookingDto bookingDto) throws Exception {
		return bookingService.getBookingByBookingId(bookingDto);
	}
	
	@ApiOperation(value = "Get booking by username")
	@RequestMapping(value = "/getbookingbyusername", method = RequestMethod.POST, produces = "application/json")
	public List<Booking> getBookingByUsername(@RequestBody BookingDto bookingDto) throws Exception {
		return bookingService.getBookingByUsername(bookingDto);
	}
	
	@ApiOperation(value = "Get booking by service")
	@RequestMapping(value = "/getbookingbyservice", method = RequestMethod.POST, produces = "application/json")
	public List<Booking> getBookingByService(@RequestBody ServiceIdDTO serviceIdDto) throws Exception {
		return bookingService.getBookingByService(serviceIdDto);
	}
	
	@ApiOperation(value = "Get booking by provider")
	@RequestMapping(value = "/getbookingbyprovider", method = RequestMethod.POST, produces = "application/json")
	public List<Booking> getBookingByProvider(@RequestBody BookingProviderDto bookingProviderDto) throws Exception {
		return bookingService.getBookingByProvider(bookingProviderDto);
	}
	
	@ApiOperation(value = "Update booking status,Date & provider by bookingId")
	@RequestMapping(value = "/updatebookingstatusdateprovider", method = RequestMethod.PUT, produces = "application/json")
	public Booking updateBookingStatusDateProvidersByBookingId(@RequestBody UpdateBookingDto updateBookingDto) throws Exception {
		return bookingService.updateBookingStatusDateProvidersByBookingId(updateBookingDto);
	}
	
	@ApiOperation(value = "Delete booking by bookingId")
	@RequestMapping(value = "/deletebooking/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Map<String, Boolean> deleteBooking(@PathVariable("id") Long id) throws Exception {
		return bookingService.deleteBooking(id);
	}
	
	
	@ApiOperation(value = "ADD user answer for booking service")
	@RequestMapping(value = "/addanswer/booking", method = RequestMethod.POST, produces = "application/json")
	public BookingAssociatedUserAnswer addUserServiceAnswer(@RequestBody BookingAssociatedUserAnswerDto userServiceAnswer) {
		return bookingService.addUserServiceAnswer(userServiceAnswer);
	}
	
	@ApiOperation(value = "GET answer of user by username")
	@RequestMapping(value = "/getanswerbyusername/booking", method = RequestMethod.POST, produces = "application/json")
	public List<BookingAssociatedUserAnswer> getUserServiceAnswerByUserName(@RequestBody BookingAssociatedUserAnswerDto userServiceAnswer)  {
		return bookingService.getUserServiceAnswerByUserName(userServiceAnswer);
	}
	
	@ApiOperation(value = "GET answer of user by bookingId")
	@RequestMapping(value = "/getanswerbybookingid/booking/{bookingid}", method = RequestMethod.GET, produces = "application/json")
	public List<BookingAssociatedUserAnswer> getUserServiceAnswerByBookingId(@PathVariable(value = "bookingid") String bookingId)  {
		return bookingService.getUserServiceAnswerByBookingId(bookingId);
	}
	
	@ApiOperation(value = "Add user comment for booking")
	@RequestMapping(value = "/addusercomment/booking", method = RequestMethod.POST, produces = "application/json")
	public BookingComment addUserCommentBooking(@RequestBody BookingCommentDto bookingCommentDto) throws Exception  {
		return bookingService.addBookingComment(bookingCommentDto);
	}
	
	@ApiOperation(value = "GET booking comment by bookingid")
	@RequestMapping(value = "/getbookingcomment/booking/{bookingid}", method = RequestMethod.GET, produces = "application/json")
	public List<BookingComment> getBookingComment(@PathVariable(value ="bookingid") String bookingId) throws Exception {
		return bookingService.getBookingCommentByBookingId(bookingId);
	}
	
	@ApiOperation(value = "Delete booking comment by bookingId")
	@RequestMapping(value = "/deletebookingcomment/booking/{bookingid}", method = RequestMethod.DELETE, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Map<String, Boolean> deleteBookingComment(@PathVariable(value = "bookingid") String bookingId) throws Exception {
		return bookingService.deleteBookingCommentByBookingId(bookingId);
	}
	
	@ApiOperation(value = "Add booking feedback")
	@RequestMapping(value = "/addbookingfeedback", method = RequestMethod.POST, produces = "application/json")
	public UserBookingFeedback addBookingFeedback(@RequestBody BookingUserFeedbackDto bookingFeedbackDto) throws Exception  {
		return bookingService.addBookingFeedback(bookingFeedbackDto);
	}
	
	@ApiOperation(value = "Update booking feedback by bookingId")
	@RequestMapping(value = "/updatebookingfeedback", method = RequestMethod.PUT, produces = "application/json")
	public UserBookingFeedback updateBookingFeedback(@RequestBody BookingUserFeedbackDto bookingFeedbackDto) throws Exception  {
		return bookingService.updateBookingFeedback(bookingFeedbackDto);
	}
	
	@ApiOperation(value = "GET booking feedback by bookingid")
	@RequestMapping(value = "/getbookingfeedback/{bookingid}", method = RequestMethod.GET, produces = "application/json")
	public UserBookingFeedback getBookingFeedback(@PathVariable(value ="bookingid") String bookingId) throws Exception {
		return bookingService.getBookingFeedback(bookingId);
	}
	
	@ApiOperation(value = "Delete booking feedback by bookingId")
	@RequestMapping(value = "/deletebookingfeedback/{bookingid}", method = RequestMethod.DELETE, produces = "application/json")
	public Map<String, Boolean> deleteBookingFeednacl(@PathVariable("bookingid") String bookingId) throws Exception {
		return bookingService.deleteBookingFeedback(bookingId);
	}
	
	
}
