package com.subhag.services.be.bookingmanagement.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.subhag.services.be.bookingmanagement.dto.ServiceAssociatedQuestionDto;
import com.subhag.services.be.bookingmanagement.dto.ServiceDto;
import com.subhag.services.be.bookingmanagement.service.ServicesService;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.ServiceAssociatedQuestion;
import com.subhag.services.be.commons.models.Services;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/v1/service")
public class ServiceController {

	@Autowired
	private ServicesService servicesService;

	@ApiOperation(value = "Register Services")
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Services addService(@RequestBody ServiceDto serviceDto) {
		return servicesService.addService(serviceDto);
	}

	@ApiOperation(value = "Available Roles")
	@RequestMapping(value = "/roles", method = RequestMethod.GET, produces = "application/json")
	public List<RoleName> getRoles() {
		return servicesService.getRoles();
	}

	@ApiOperation(value = "Get a Particular Services")
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Services getService(@PathVariable(value = "id") String serviceId) {
		return servicesService.getService(serviceId);
	}

	@ApiOperation(value = "Get all available Services")
	@GetMapping(value = "/list")	
	public List<Services> getServicesList() {
		return  servicesService.getServiceList();
	}

	@PutMapping(value = "/update")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public ResponseEntity<Services> updateService(@RequestBody Services serviceToBeUpdated) throws Exception {
		return ResponseEntity.ok(servicesService.updateService(serviceToBeUpdated));
	}
	
	@ApiOperation(value = "DELETE service by serviceId")
	@RequestMapping(value = "/deleteservice/{serviceId}", method = RequestMethod.DELETE, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Map<String, Boolean> deleteService(@PathVariable(value = "serviceId") String serviceId) throws Exception {
		return servicesService.deleteService(serviceId);
	}
	
	@ApiOperation(value = "ADD Question Associated To Service")
	@RequestMapping(value = "/addquestion/service", method = RequestMethod.POST, produces = "application/json")
	public ServiceAssociatedQuestion addQuestionAssociatedToService(@RequestBody ServiceAssociatedQuestionDto associatedQuestion) {
		return servicesService.addQuestionRelatedToService(associatedQuestion);
	}
	
	@ApiOperation(value = "UPDATE Question Associated To Service")
	@RequestMapping(value = "/updatequestion/service", method = RequestMethod.PUT, produces = "application/json")
	public ServiceAssociatedQuestion updateQuestionAssociatedToService(@RequestBody ServiceAssociatedQuestion associatedQuestion) throws Exception {
		return servicesService.updateQuestionRelatedToService(associatedQuestion);
	}
	
	@ApiOperation(value = "GET Question Associated To Service")
	@RequestMapping(value = "/getquestion/service/{id}", method = RequestMethod.GET, produces = "application/json")
	public ServiceAssociatedQuestion updateQuestionAssociatedToService(@PathVariable(value = "id") Long Id) throws Exception {
		return servicesService.getQuestionRelatedToService(Id);
	}
	
	@ApiOperation(value = "DELETE Question Associated To Service")
	@RequestMapping(value = "/deletequestion/service/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public Map<String, Boolean> deleteQuestionAssociatedToService(@PathVariable(value = "id") Long Id) throws Exception {
		return servicesService.deleteQuestionRelatedToService(Id);
	}
	
	@ApiOperation(value = "GET Question Associated To Service By ServiceId")
	@RequestMapping(value = "/getquestionbyserviceid/service/{serviceid}", method = RequestMethod.GET, produces = "application/json")
	public List<ServiceAssociatedQuestion> updateQuestionAssociatedToService(@PathVariable(value = "serviceid") String serviceId) throws Exception {
		return servicesService.getQuestionByServiceId(serviceId);
	}
	
	@ApiOperation(value = "GET All Questions")
	@RequestMapping(value = "/getallquestion/service", method = RequestMethod.GET, produces = "application/json")
	public List<ServiceAssociatedQuestion> getAllQuestion() throws Exception {
		return servicesService.getAllQuestion();
	}

}
