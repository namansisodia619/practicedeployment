package com.subhag.services.be.bookingmanagement.dto;


public class BookingAssociatedUserAnswerDto {
	
	private String userName;
	
	private String bookingId;
	
	private String answer;
	
	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	
	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}
	
	public String getanswer() {
		return answer;
	}

	public void setanswer(String answer) {
		this.answer = answer;
	}

}
