package com.subhag.services.be.bookingmanagement.dto;


public class BookingCommentDto {
	
	private String bookingId;
	
	private String userName;
	
	private String bookingComment;

	
	public String getbookingId() {
		return bookingId;
	}

	public void setbookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}
	
	public String getbookingComment() {
		return bookingComment;
	}

	public void setbookingComment(String bookingComment) {
		this.bookingComment = bookingComment;
	}

}
