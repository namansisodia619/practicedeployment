package com.subhag.services.be.bookingmanagement.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class BookingDetailDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	
	private String  userName;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	private String services;
	
	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}

	private LocalDateTime bookingDate;
	
	

	
}
