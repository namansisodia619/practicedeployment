package com.subhag.services.be.bookingmanagement.dto;

public class BookingOngoingDto {
	
	private String bookingId;

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
}
