package com.subhag.services.be.bookingmanagement.dto;

public class BookingProviderDto {
	
	private String userName;
	
	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}
}
