package com.subhag.services.be.bookingmanagement.dto;

import java.time.LocalDateTime;
import java.util.List;

public class BookingScheduledDto {

	private LocalDateTime bookingDate;
	
	private String bookingId;
	
	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}
	
}

