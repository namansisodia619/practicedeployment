package com.subhag.services.be.bookingmanagement.dto;

import javax.persistence.Column;

public class BookingUserFeedbackDto {
	
	private String bookingId;
	
	private int rating;
	
	private String bookingFeedback;
	
	public String getbookingId() {
		return bookingId;
	}

	public void setbookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	public int getrating() {
		return rating;
	}

	public void setrating(int rating) {
		this.rating = rating;
	}
	
	public String getbookingFeedback() {
		return bookingFeedback;
	}

	public void setbookingFeedback(String bookingFeedback) {
		this.bookingFeedback = bookingFeedback;
	}


}
