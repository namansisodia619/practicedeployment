package com.subhag.services.be.bookingmanagement.dto;

import javax.persistence.Column;

public class ServiceAssociatedQuestionDto {

	private Long Id;
	
	private String serviceId;

	private String question;
	
	private String questionType;
	
	private String options;
	
	public Long getId() {
		return Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}
	
	public String getserviceId() {
		return serviceId;
	}

	public void setserviceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getquestion() {
		return question;
	}

	public void setquestion(String question) {
		this.question = question;
	}
	
	public String getquestionType() {
		return questionType;
	}

	public void setquestionType(String questionType) {
		this.questionType = questionType;
	}
	
	public String getoptions() {
		return options;
	}

	public void setoptions(String options) {
		this.options = options;
	}
	
}
