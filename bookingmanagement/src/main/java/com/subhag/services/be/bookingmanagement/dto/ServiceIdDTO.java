package com.subhag.services.be.bookingmanagement.dto;

public class ServiceIdDTO {
	
	private String serviceId;
	private String userName;

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}
	

}
