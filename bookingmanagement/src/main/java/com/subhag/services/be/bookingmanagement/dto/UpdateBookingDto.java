package com.subhag.services.be.bookingmanagement.dto;

import java.time.LocalDateTime;
import java.util.List;

public class UpdateBookingDto {
	
	private LocalDateTime bookingDate;
	private String bookingId;
	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	private List<String> providers;
	private String bookingStatus;
	
	
	public List<String> getProviders() {
		return providers;
	}

	public void setProviders(List<String> providers) {
		this.providers = providers;
	}

	


	public LocalDateTime getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(LocalDateTime bookingDate) {
		this.bookingDate = bookingDate;
	}

	
	public String getbookingStatus() {
		return bookingStatus;
	}

	public void setbookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

}
