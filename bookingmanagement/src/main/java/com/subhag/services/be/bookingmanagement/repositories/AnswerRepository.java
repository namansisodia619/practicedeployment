package com.subhag.services.be.bookingmanagement.repositories;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.subhag.services.be.commons.models.BookingAssociatedUserAnswer;
import com.subhag.services.be.commons.models.ServiceAssociatedQuestion;


@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")
/***
 * 
 * @author Naman sisodia
 *
 */
public interface AnswerRepository extends JpaRepository<BookingAssociatedUserAnswer, Long> {
	
	List<BookingAssociatedUserAnswer> findByUserName(String userName);
	List<BookingAssociatedUserAnswer> findByBookingId(String bookingId);
}
