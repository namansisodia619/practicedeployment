package com.subhag.services.be.bookingmanagement.repositories;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.subhag.services.be.commons.models.Booking;
import com.subhag.services.be.commons.models.BookingComment;
import com.subhag.services.be.commons.models.UserBookingFeedback;

@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")
public interface BookingFeedbackRepository extends JpaRepository<UserBookingFeedback, Long> {

	UserBookingFeedback findByBookingId(String bookingId);

}
