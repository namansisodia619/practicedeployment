package com.subhag.services.be.bookingmanagement.repositories;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.subhag.services.be.commons.models.Booking;
import com.subhag.services.be.commons.models.ServiceAssociatedQuestion;
import com.subhag.services.be.commons.models.User;

@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")
/***
 * 
 * @author Naman sisodia
 *
 */
public interface QuestionRepository extends JpaRepository<ServiceAssociatedQuestion, Long> {
	
	List<ServiceAssociatedQuestion> findByServiceId(String ServiceId);

}
