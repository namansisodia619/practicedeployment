package com.subhag.services.be.bookingmanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.commons.models.RoleName;

public interface RoleNameRepository extends JpaRepository<RoleName, Long> {
 RoleName findByRoleName(String roleName);
}
