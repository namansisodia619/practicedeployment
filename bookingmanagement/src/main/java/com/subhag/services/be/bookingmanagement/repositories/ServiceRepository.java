package com.subhag.services.be.bookingmanagement.repositories;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.subhag.services.be.commons.models.Services;

@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")

/***
 * 
 * @author Nikhil
 *
 */

public interface ServiceRepository extends JpaRepository<Services, Long> {
	Services findByServiceId(String serviceId);

}
