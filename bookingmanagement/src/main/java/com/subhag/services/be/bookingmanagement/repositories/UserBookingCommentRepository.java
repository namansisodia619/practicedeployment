package com.subhag.services.be.bookingmanagement.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.subhag.services.be.bookingmanagement.dto.BookingCommentDto;
import com.subhag.services.be.commons.models.BookingComment;

public interface UserBookingCommentRepository extends JpaRepository<BookingComment, Long> {
	
	BookingComment findByBookingId(String bookingId);
	
	 @Query("SELECT b FROM BookingComment b WHERE b.bookingId=:bookingId")
	List<BookingComment> findAllByBookingId(String bookingId);

	
}
