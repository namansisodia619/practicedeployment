package com.subhag.services.be.bookingmanagement.repositories;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.subhag.services.be.commons.models.User;


@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")

/***
 * 
 * @author Rajesh V
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

  User findByUserName(String userName);
  
  @Query("Select c from User c where c.userType = :userTypeAdmin or c.userType = :userTypeManager")
  List<User> findByUserType(@Param("userTypeAdmin")String userTypeAdmin,@Param("userTypeManager")String userTypeManager);
}

