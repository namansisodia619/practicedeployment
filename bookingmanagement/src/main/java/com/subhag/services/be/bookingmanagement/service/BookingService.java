package com.subhag.services.be.bookingmanagement.service;

import java.util.List;
import java.util.Map;

import com.subhag.services.be.bookingmanagement.dto.BookingAssignedDto;
import com.subhag.services.be.bookingmanagement.dto.BookingAssociatedUserAnswerDto;
import com.subhag.services.be.bookingmanagement.dto.BookingCommentDto;
import com.subhag.services.be.bookingmanagement.dto.BookingCompleteDto;
import com.subhag.services.be.bookingmanagement.dto.BookingDetailDto;
import com.subhag.services.be.bookingmanagement.dto.BookingDto;
import com.subhag.services.be.bookingmanagement.dto.BookingOngoingDto;
import com.subhag.services.be.bookingmanagement.dto.BookingProviderDto;
import com.subhag.services.be.bookingmanagement.dto.BookingScheduledDto;
import com.subhag.services.be.bookingmanagement.dto.BookingUserFeedbackDto;
import com.subhag.services.be.bookingmanagement.dto.ServiceIdDTO;
import com.subhag.services.be.bookingmanagement.dto.UpdateBookingDto;
import com.subhag.services.be.commons.models.Booking;
import com.subhag.services.be.commons.models.BookingAssociatedUserAnswer;
import com.subhag.services.be.commons.models.BookingComment;
import com.subhag.services.be.commons.models.UserBookingFeedback;

public interface BookingService {
	Booking bookingNew(BookingDto bookingDto) throws Exception;
	Booking bookingAssigned(BookingAssignedDto bookingAssignedDto) throws Exception;
	Booking bookingScheduled(BookingScheduledDto bookingScheduledDto) throws Exception;
	Booking bookingOngoing(BookingOngoingDto bookingOngoingDto) throws Exception;
	Booking bookingComplete(BookingCompleteDto bookingCompleteDto) throws Exception;
	List<Booking> getAllBooking();
	Booking getBookingByBookingId(BookingDto bookingDto) throws Exception;
	List<Booking> getBookingByUsername(BookingDto bookingDto) throws Exception;
	List<Booking> getBookingByService(ServiceIdDTO serviceIdDto) throws Exception;
	List<Booking> getBookingByProvider(BookingProviderDto bookingProviderDto) throws Exception;
	Booking updateBookingStatusDateProvidersByBookingId(UpdateBookingDto updateBookingDto) throws Exception;
	Map<String, Boolean> deleteBooking(Long id) throws Exception;
	BookingAssociatedUserAnswer addUserServiceAnswer(BookingAssociatedUserAnswerDto userServiceAnswer);
	List<BookingAssociatedUserAnswer> getUserServiceAnswerByUserName(BookingAssociatedUserAnswerDto userServiceAnswer);
	List<BookingAssociatedUserAnswer> getUserServiceAnswerByBookingId(String bookingId);
	BookingComment addBookingComment(BookingCommentDto bookingCommentDto) throws Exception;
	List<BookingComment> getBookingCommentByBookingId(String bookingId) throws Exception;
	Map<String, Boolean> deleteBookingCommentByBookingId(String bookingId) throws Exception;
	UserBookingFeedback addBookingFeedback(BookingUserFeedbackDto bookingFeedbackDto) throws Exception;
	UserBookingFeedback updateBookingFeedback(BookingUserFeedbackDto bookingFeedbackDto) throws Exception;
	UserBookingFeedback getBookingFeedback(String bookingId) throws Exception;
	Map<String, Boolean> deleteBookingFeedback(String bookingId) throws Exception;

}
