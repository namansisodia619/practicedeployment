package com.subhag.services.be.bookingmanagement.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subhag.services.be.bookingmanagement.config.EmailService;
import com.subhag.services.be.bookingmanagement.config.MSG91MessageConfig;
import com.subhag.services.be.bookingmanagement.dto.BookingAssignedDto;
import com.subhag.services.be.bookingmanagement.dto.BookingAssociatedUserAnswerDto;
import com.subhag.services.be.bookingmanagement.dto.BookingCommentDto;
import com.subhag.services.be.bookingmanagement.dto.BookingCompleteDto;
import com.subhag.services.be.bookingmanagement.dto.BookingDto;
import com.subhag.services.be.bookingmanagement.dto.BookingOngoingDto;
import com.subhag.services.be.bookingmanagement.dto.BookingProviderDto;
import com.subhag.services.be.bookingmanagement.dto.BookingScheduledDto;
import com.subhag.services.be.bookingmanagement.dto.BookingUserFeedbackDto;
import com.subhag.services.be.bookingmanagement.dto.ServiceIdDTO;
import com.subhag.services.be.bookingmanagement.dto.UpdateBookingDto;
import com.subhag.services.be.bookingmanagement.exception.ResourceNotFoundException;
import com.subhag.services.be.bookingmanagement.repositories.AnswerRepository;
import com.subhag.services.be.bookingmanagement.repositories.BookingFeedbackRepository;
import com.subhag.services.be.bookingmanagement.repositories.BookingRepository;
import com.subhag.services.be.bookingmanagement.repositories.ServiceRepository;
import com.subhag.services.be.bookingmanagement.repositories.UserBookingCommentRepository;
import com.subhag.services.be.bookingmanagement.repositories.UserRepository;
import com.subhag.services.be.commons.models.Booking;
import com.subhag.services.be.commons.models.BookingAssociatedUserAnswer;
import com.subhag.services.be.commons.models.BookingComment;
import com.subhag.services.be.commons.models.BookingStatus;
import com.subhag.services.be.commons.models.Mail;
import com.subhag.services.be.commons.models.Services;
import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.commons.models.UserBookingFeedback;

@Service
public class BookingServiceImpl implements BookingService {
	
	@Autowired
	private BookingRepository bookingRepository;
	
	@Autowired
	private ServiceRepository serviceRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private AnswerRepository answerRepository;
	
	@Autowired
	private UserBookingCommentRepository userCommentRepository;
	
	@Autowired
	private BookingFeedbackRepository feedbackRepository;
	
	private MSG91MessageConfig messageService;


	@Override
	public Booking bookingNew(BookingDto bookingDto) throws Exception {
		Booking booking = new Booking();
		
		User user = userRepository.findByUserName(bookingDto.getUserName());
		
		if (user == null) {
			throw new ResourceNotFoundException("User not found");
		}
		booking.setUser(user);
		
		booking.setBookingDate(bookingDto.getBookingDate());
		booking.setBookingStatus(BookingStatus.NEW.toString());
		bookingDto.setbookingStatus(BookingStatus.NEW.toString());
		booking.setCreatedBy(bookingDto.getUserName());
		booking.setDateCreated(LocalDateTime.now());
		booking.setServices(serviceRepository.findByServiceId(bookingDto.getServiceId()));
		
		//add admin manager email and phone number
		String mobiles="";
		for (User userAdminList : userRepository.findByUserType("ADMIN","MANAGER")) {
			mobiles = "+91"+userAdminList.getUserPhoneNumber()+",";
		}
		//add user email and phone number
		mobiles += "+91"+user.getUserPhoneNumber();
		

		Booking bookingToBeReturned = bookingRepository.save(booking);
		String bookingID="";
		Long id=500l+bookingToBeReturned.getId();
		if(bookingDto.getServiceId().equalsIgnoreCase("IUI@HOME")) {
			bookingID="CONCEIVE@"+id+"-I";
		}else if(bookingDto.getServiceId().equalsIgnoreCase("CONSULTATION"))
		{
			bookingID="CONCEIVE@"+id+"-C";
		}else if((bookingDto.getServiceId().equalsIgnoreCase("SEMEN-ANALYSIS")))
		{
			bookingID="CONCEIVE@"+id+"-SA";
		}else {
			throw new Exception("Not a Valid Service ID");
		}
		bookingToBeReturned.setBookingId(bookingID);
		bookingToBeReturned=bookingRepository.save(bookingToBeReturned);
		bookingDto.setBookingId(bookingID);
		
		if (bookingToBeReturned != null) {
			Mail mail = new Mail();
			mail.setFrom("noreply@subhag.in");
			mail.setSubject("Subhag Service- Booking Number:-" + bookingDto.getBookingId()+" Status :"+bookingDto.getbookingStatus());	
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("serviceName", serviceRepository.findByServiceId(bookingDto.getServiceId()).getTitle());
			model.put("name", userRepository.findByUserName(bookingDto.getUserName()).getFirstName());
			model.put("bookingDate", bookingDto.getBookingDate());
			model.put("bookingID", bookingDto.getBookingId());
			model.put("bookingStatus", bookingDto.getbookingStatus());
			model.put("location", "Bangalore");
			model.put("signature", "http://subhag.in");
			mail.setModel(model);
			
			List<String> emails=new ArrayList<>();
			for (User userAdminList : userRepository.findByUserType("ADMIN","MANAGER")) {
				emails.add(userAdminList.getUserName());
			}
			mail.setTo((emails.stream().toArray(String[]::new)));
			emailService.sendBookingConfirmationToAdminManager(mail);
			emails.clear();
			emails.add(bookingDto.getUserName());
			//add user email and phone number
			mail.setTo((emails.stream().toArray(String[]::new)));
			emailService.sendBookingConfirmationToCustomer(mail);
			
		//	sendSMSForBooking(mobiles,bookingDto);
		} else {
			throw new ResourceNotFoundException("Unable to save booking");
		}

		return bookingToBeReturned;
	}

	@Override
	public Booking bookingAssigned(BookingAssignedDto bookingAssignedDto) throws Exception {
		// TODO Auto-generated method stub
		Booking booking=bookingRepository.findByBookingId(bookingAssignedDto.getBookingId());
		if(booking==null) {
			throw new ResourceNotFoundException("Specified Booking not found");
		}
		
		List<User> providers=new ArrayList<>();
		for (String user : bookingAssignedDto.getProviders()) {
			providers.add(userRepository.findByUserName(user));
		}
		booking.setProvider(providers);
		booking.setBookingStatus(BookingStatus.ASSIGNED.toString());
		
		
		Booking bookingToBeReturned=bookingRepository.save(booking);
		if (bookingToBeReturned == null) {
			throw new ResourceNotFoundException("Unable to save booking");
		}
		return bookingToBeReturned;
	}
	
	@Override
	public Booking bookingScheduled(BookingScheduledDto bookingScheduledDto) throws Exception {
		// TODO Auto-generated method stub
		Booking booking=bookingRepository.findByBookingId(bookingScheduledDto.getBookingId());
		if(booking==null) {
			throw new ResourceNotFoundException("Specified Booking not found");
		}
		
	    booking.setBookingDate(bookingScheduledDto.getBookingDate());
		booking.setBookingStatus(BookingStatus.SCHEDULED.toString());
		
		
		Booking bookingToBeReturned=bookingRepository.save(booking);
		
		if (bookingToBeReturned == null) {
			throw new ResourceNotFoundException("Unable to save booking");
		} else  {
			Mail mail = new Mail();
			mail.setFrom("noreply@subhag.in");
			mail.setSubject("Subhag Service- Booking Number:-" + bookingScheduledDto.getBookingId()+" Status :"+BookingStatus.SCHEDULED.toString());	
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("bookingDate", bookingScheduledDto.getBookingDate());
			model.put("bookingID", bookingScheduledDto.getBookingId());
			model.put("location", "Bangalore");
			model.put("signature", "http://subhag.in");
			mail.setModel(model);
				
			List<String> emails=new ArrayList<>();
			for (User bookingProvider : bookingToBeReturned.getProvider()) {
				emails.add(bookingProvider.getUserName());
			}
			mail.setTo((emails.stream().toArray(String[]::new)));
			emailService.sendBookingAppointmentToGYNC(mail);
			emails.clear();
			
			emails.add(bookingToBeReturned.getUser().getUserName());
			mail.setTo((emails.stream().toArray(String[]::new)));
			emailService.sendBookingAppointmentToCustomer(mail);
			
		//	sendSMSForBooking(mobiles,bookingDto);
		}

		return bookingToBeReturned;
	}
	
	@Override
	public Booking bookingOngoing(BookingOngoingDto bookingOngoingDto) throws Exception {
		// TODO Auto-generated method stub
		Booking booking=bookingRepository.findByBookingId(bookingOngoingDto.getBookingId());
		if(booking==null) {
			throw new ResourceNotFoundException("Specified Booking not found");
		}
		booking.setBookingStatus(BookingStatus.ONGOING.toString());
		
		Booking bookingToBeReturned=bookingRepository.save(booking);
		if (bookingToBeReturned == null) {
			throw new ResourceNotFoundException("Unable to save booking");
		}
		return bookingToBeReturned;
	}
	
	
	@Override
	public Booking bookingComplete(BookingCompleteDto bookingCompleteDto) throws Exception {
		// TODO Auto-generated method stub
		Booking booking=bookingRepository.findByBookingId(bookingCompleteDto.getBookingId());
		if(booking==null) {
			throw new ResourceNotFoundException("Specified Booking not found");
		}
		booking.setBookingStatus(BookingStatus.COMPLETE.toString());
		
		Booking bookingToBeReturned=bookingRepository.save(booking);
		if (bookingToBeReturned == null) {
			throw new ResourceNotFoundException("Unable to save booking");
		} else {
			Mail mail = new Mail();
			mail.setFrom("noreply@subhag.in");
			mail.setSubject("Subhag Service- Booking Number:-" + bookingCompleteDto.getBookingId()+" Status :"+BookingStatus.COMPLETE.toString());	
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("name", bookingToBeReturned.getUser().getFirstName());
			model.put("bookingID", bookingCompleteDto.getBookingId());
			model.put("location", "Bangalore");
			model.put("signature", "http://subhag.in");
			mail.setModel(model);
				
			List<String> emails=new ArrayList<>();
			
			emails.add(bookingToBeReturned.getUser().getUserName());
			mail.setTo((emails.stream().toArray(String[]::new)));
			emailService.sendBookingCompleteToCustomer(mail);
			
		//	sendSMSForBooking(mobiles,bookingDto);

		}
		
		return bookingToBeReturned;
	}
    
	//method for sending message to multiple mobiles(send comma seperated mobile number)
	private void sendSMSForBooking(String mobiles,BookingDto bookingDto) {
		
		//message template for sending booking information
		String messageTemplate = "Subhag Service- Booking Number:-" + bookingDto.getBookingId()+" Status :"+bookingDto.getbookingStatus(); 
		messageTemplate += "\nSubhag Service- Booking Number:- "+bookingDto.getBookingId();
		messageTemplate += "\nserviceName: "+serviceRepository.findByServiceId(bookingDto.getServiceId()).getTitle();
        messageTemplate += "\nname: "+userRepository.findByUserName(bookingDto.getUserName()).getFirstName();
        messageTemplate += "\nbookingDate: "+bookingDto.getBookingDate();
        messageTemplate += "\nbookingID: "+bookingDto.getBookingId();
        messageTemplate += "\nbookingStatus: "+bookingDto.getbookingStatus();
        messageTemplate += "\nlocation: Bangalore";
        messageTemplate += "\nsignature: http://subhag.in";

		
        //define route
        String route="4";

        //Prepare Url
        URLConnection myURLConnection=null;
        URL myURL=null;
        BufferedReader reader=null;

        //encoding message
        String encoded_message=URLEncoder.encode(messageTemplate);
        
      //Send SMS API
        String mainUrl="http://api.msg91.com/api/sendhttp.php?";
        //Prepare parameter string
        StringBuilder sbPostData= new StringBuilder(mainUrl);
        sbPostData.append("authkey="+messageService.AUTH_KEY);
        sbPostData.append("&mobiles="+mobiles);
        sbPostData.append("&message="+encoded_message);
        sbPostData.append("&route="+route);
        sbPostData.append("&sender="+messageService.SENDER_ID);
        //final string
        mainUrl = sbPostData.toString();
        try
        {
            //prepare connection
            myURL = new URL(mainUrl);
            myURLConnection = myURL.openConnection();
            myURLConnection.connect();
            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            //reading response
            String response;
            while ((response = reader.readLine()) != null) 
            //print response
            System.out.println(response);

            //finally close connection
            reader.close();
        }
        catch (IOException e)
        {
        		System.out.println("error");
                e.printStackTrace();
        }
	}
	
	
	
		
	@Override
	public Booking getBookingByBookingId(BookingDto bookingDto) throws Exception {
		Booking booking=bookingRepository.findByBookingId(bookingDto.getBookingId());
		if(booking==null) {
			throw new ResourceNotFoundException("Specified Booking not found");
		}
		
		return booking;
	}
	
	@Override
	public List<Booking> getBookingByUsername(BookingDto bookingDto) throws Exception {
		User user = userRepository.findByUserName(bookingDto.getUserName());
		if(user==null) {
			throw new ResourceNotFoundException("User not found");
		}
		return bookingRepository.findByUser(user);
	}
	
	@Override
	public List<Booking> getBookingByService(ServiceIdDTO serviceIdDto) throws Exception {
		Services services = serviceRepository.findByServiceId(serviceIdDto.getServiceId());
		if(services==null) {
			throw new ResourceNotFoundException("Service not found");
		}
		User user = userRepository.findByUserName(serviceIdDto.getuserName());
		
		if(user != null && user.getUserType().contains("PROVIDER"))  {
			Iterator<Booking> bookingList = bookingRepository.findByProvider(user).iterator();
			List<Booking> serviceBookingList = new ArrayList<Booking>();
			while(bookingList.hasNext()) {
				Booking booking = bookingList.next();
				if(booking.getServices().getId().equals(services.getId()))
					serviceBookingList.add(booking);
			}
			return serviceBookingList;
		}
		
		return bookingRepository.findByServices(services);
	}
	
	@Override
	public List<Booking> getBookingByProvider(BookingProviderDto bookingProviderDto) throws Exception {
		User user = userRepository.findByUserName(bookingProviderDto.getuserName());
		if(user==null) {
			throw new ResourceNotFoundException("User not found");
		}
		return bookingRepository.findByProvider(user);
	}
	
	@Override
	public List<Booking> getAllBooking() {
		return bookingRepository.findAll();
	}
	
	@Override
	public Booking updateBookingStatusDateProvidersByBookingId(UpdateBookingDto updateBookingDto) throws Exception {
		// TODO Auto-generated method stub
		Booking booking=bookingRepository.findByBookingId(updateBookingDto.getBookingId());
		if(booking==null) {
			throw new ResourceNotFoundException("Specified Booking not found");
		}
		booking.setBookingStatus(updateBookingDto.getbookingStatus());
		booking.setBookingDate(updateBookingDto.getBookingDate());
	
		List<User> providers=new ArrayList<>();
		for (String user : updateBookingDto.getProviders()) {
			providers.add(userRepository.findByUserName(user));
		}
		booking.setProvider(providers);
		
		Booking bookingToBeReturned=bookingRepository.save(booking);
		
		if(bookingToBeReturned ==  null) {
			throw new ResourceNotFoundException("something happen booking not updated");
		}
		return bookingToBeReturned;
	}
	
	@Override
	public Map<String, Boolean> deleteBooking(Long id) throws Exception {
		Booking booking=bookingRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Booking not found"));
		bookingRepository.delete(booking);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@Override
	public BookingAssociatedUserAnswer addUserServiceAnswer(BookingAssociatedUserAnswerDto userServiceAnswer) {
		List<BookingAssociatedUserAnswer> userAnswerList = answerRepository.findByBookingId(userServiceAnswer.getBookingId());
		
		BookingAssociatedUserAnswer userAnswer;
		if(userAnswerList.size() == 0) 
			userAnswer = new BookingAssociatedUserAnswer();
		else 
			userAnswer = userAnswerList.get(0);
		userAnswer.setanswer(userServiceAnswer.getanswer());
		userAnswer.setbookingId(userServiceAnswer.getBookingId());
		userAnswer.setuserName(userServiceAnswer.getuserName());
		return answerRepository.save(userAnswer);
	}
	
	@Override
	public List<BookingAssociatedUserAnswer> getUserServiceAnswerByUserName(BookingAssociatedUserAnswerDto userServiceAnswer) {
		return answerRepository.findByUserName(userServiceAnswer.getuserName());
	}

	@Override
	public List<BookingAssociatedUserAnswer> getUserServiceAnswerByBookingId(String bookingId) {
		return answerRepository.findByBookingId(bookingId);
	}
	
	@Override
	public BookingComment addBookingComment(BookingCommentDto bookingCommentDto) throws Exception {	
			BookingComment bookingComment=new BookingComment();
			bookingComment.setbookingId(bookingCommentDto.getbookingId());
			bookingComment.setBookingComment(bookingCommentDto.getbookingComment());
			bookingComment.setUserName(bookingCommentDto.getuserName());
		return userCommentRepository.save(bookingComment);
	}
	
	@Override
	public  List<BookingComment> getBookingCommentByBookingId(String bookingId) throws Exception {
		return userCommentRepository.findAllByBookingId(bookingId);
	}
	
	@Override
	public Map<String, Boolean> deleteBookingCommentByBookingId(String bookingId) throws Exception {
		BookingComment bookingComment=userCommentRepository.findByBookingId(bookingId);	
		if(bookingComment == null) 
			throw new ResourceNotFoundException("Booking comment not found");
		
		userCommentRepository.delete(bookingComment);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@Override
	public UserBookingFeedback addBookingFeedback(BookingUserFeedbackDto bookingFeedbackDto) throws Exception {
		// TODO Auto-generated method stub
		UserBookingFeedback userFeedback=feedbackRepository.findByBookingId(bookingFeedbackDto.getbookingId());
		
		if(userFeedback != null) 
			throw new ResourceNotFoundException("userFeedback already exist");
		
		userFeedback = new UserBookingFeedback();
		userFeedback.setbookingId(bookingFeedbackDto.getbookingId());
		userFeedback.setrating(bookingFeedbackDto.getrating());
		userFeedback.setbookingFeedback(bookingFeedbackDto.getbookingFeedback());
		return feedbackRepository.save(userFeedback);
	}
	
	@Override
	public UserBookingFeedback updateBookingFeedback(BookingUserFeedbackDto bookingFeedbackDto) throws Exception {
		// TODO Auto-generated method stub
		UserBookingFeedback userFeedback=feedbackRepository.findByBookingId(bookingFeedbackDto.getbookingId());
		
		if(userFeedback == null) 
			throw new ResourceNotFoundException("userFeedback not found");
		
		userFeedback.setbookingId(bookingFeedbackDto.getbookingId());
		userFeedback.setrating(bookingFeedbackDto.getrating());
		userFeedback.setbookingFeedback(bookingFeedbackDto.getbookingFeedback());
		return feedbackRepository.save(userFeedback);
	}

	
	@Override
	public  UserBookingFeedback getBookingFeedback(String bookingId) throws Exception {
		return feedbackRepository.findByBookingId(bookingId);
	}
	
	@Override
	public Map<String, Boolean> deleteBookingFeedback(String bookingId) throws Exception {
		UserBookingFeedback userFeedback=feedbackRepository.findByBookingId(bookingId);
		
		if(userFeedback == null) 
			throw new ResourceNotFoundException("userFeedback not found");
		
		feedbackRepository.delete(userFeedback);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
