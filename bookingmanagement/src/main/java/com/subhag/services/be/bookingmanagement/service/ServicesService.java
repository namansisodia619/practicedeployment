package com.subhag.services.be.bookingmanagement.service;

import java.util.List;
import java.util.Map;

import com.subhag.services.be.bookingmanagement.dto.ServiceAssociatedQuestionDto;
import com.subhag.services.be.bookingmanagement.dto.ServiceDto;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.ServiceAssociatedQuestion;
import com.subhag.services.be.commons.models.Services;

public interface ServicesService {
	Services addService(ServiceDto serviceDto);
	Services getService(String serviceId);
	Services updateService(Services servicesToBeUpdated) throws Exception;
	Map<String, Boolean> deleteService(String serviceId) throws Exception;
	List<Services> getServiceList();
	List<RoleName> getRoles();
	ServiceAssociatedQuestion addQuestionRelatedToService(ServiceAssociatedQuestionDto associatedQuestion);
	ServiceAssociatedQuestion updateQuestionRelatedToService(ServiceAssociatedQuestion associatedQuestion) throws Exception;
	ServiceAssociatedQuestion getQuestionRelatedToService(Long Id) throws Exception;
	Map<String, Boolean> deleteQuestionRelatedToService(Long Id) throws Exception;
	List<ServiceAssociatedQuestion> getQuestionByServiceId(String serviceId) throws Exception;
	List<ServiceAssociatedQuestion> getAllQuestion() throws Exception;
}
