package com.subhag.services.be.bookingmanagement.service;

import java.util.ArrayList;



import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subhag.services.be.bookingmanagement.dto.ServiceAssociatedQuestionDto;
import com.subhag.services.be.bookingmanagement.dto.ServiceDto;
import com.subhag.services.be.bookingmanagement.exception.ResourceNotFoundException;
import com.subhag.services.be.bookingmanagement.repositories.QuestionRepository;
import com.subhag.services.be.bookingmanagement.repositories.RoleNameRepository;
import com.subhag.services.be.bookingmanagement.repositories.ServiceRepository;
import com.subhag.services.be.commons.models.Services;
import com.subhag.services.be.commons.models.BookingStatus;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.ServiceAssociatedQuestion;

@Service
public class ServicesServiceImpl implements ServicesService {

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	private RoleNameRepository roleRepository;
	
	@Autowired
	private QuestionRepository questionRepository;

	@Override
	public Services addService(ServiceDto serviceDto) {
		Services service = new Services();
		List<RoleName> rolesAssosciated = new ArrayList<>();
		service.setServiceId(serviceDto.getServiceId());
		service.setTitle(serviceDto.getServiceTitle());
		service.setDescription(serviceDto.getServiceDescription());
		service.setServiceFee(serviceDto.getServiceFee());
		for (String roleName : serviceDto.getRequiredRoles()) {
			rolesAssosciated.add(roleRepository.findByRoleName(roleName));
		}
		service.setAssociatedRoles(rolesAssosciated);
		return serviceRepository.save(service);
	}

	@Override
	public Services getService(String serviceId) {
		Services service = serviceRepository.findByServiceId(serviceId);

		// Throws exception if service not found
		if (service == null) {
			try {
				throw new Exception("Service not available");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return service;
	}

	@Override
	public Services updateService(Services serviceToBeUpdated) throws Exception {

		Services service = serviceRepository.findByServiceId(serviceToBeUpdated.getServiceId());
		if (service == null) {
			throw new Exception("Service not found");
		}
		serviceToBeUpdated.setId(service.getId());

		return serviceRepository.save(serviceToBeUpdated);

	}
	
	@Override
	public Map<String, Boolean> deleteService(String serviceId) throws Exception {
		Services service =  serviceRepository.findByServiceId(serviceId);
				
		if(service == null)
			throw new ResourceNotFoundException("Service not found");
		serviceRepository.delete(service);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	@Override
	public List<Services> getServiceList() {
		return serviceRepository.findAll();
	}

	public List<RoleName> getRoles() {
		// TODO Auto-generated method stub
		return roleRepository.findAll();
	}

	@Override
	public ServiceAssociatedQuestion addQuestionRelatedToService(ServiceAssociatedQuestionDto associatedQuestion) {
		ServiceAssociatedQuestion serviceAssociatedQuestion = new ServiceAssociatedQuestion();
		serviceAssociatedQuestion.setserviceId(associatedQuestion.getserviceId());
		serviceAssociatedQuestion.setquestion(associatedQuestion.getquestion());
		if(associatedQuestion.getquestionType().equals("MCQ")) {
			serviceAssociatedQuestion.setquestionType(associatedQuestion.getquestionType());
			serviceAssociatedQuestion.setoptions(associatedQuestion.getoptions());
		} else {
			serviceAssociatedQuestion.setquestionType(associatedQuestion.getquestionType());
			serviceAssociatedQuestion.setoptions(null);
		}
		return questionRepository.save(serviceAssociatedQuestion);
	}
	
	@Override
	public ServiceAssociatedQuestion updateQuestionRelatedToService(ServiceAssociatedQuestion associatedQuestion) throws Exception {
		ServiceAssociatedQuestion existingQuestion = questionRepository.findById(associatedQuestion.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Question not found"));
		
		return questionRepository.save(associatedQuestion);
	}
	
	@Override
	public ServiceAssociatedQuestion getQuestionRelatedToService(Long Id) throws Exception {
		ServiceAssociatedQuestion existingQuestion = questionRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Question not found"));
		
		return existingQuestion;
	}
	
	@Override
	public Map<String, Boolean> deleteQuestionRelatedToService(Long Id) throws Exception {
		ServiceAssociatedQuestion existingQuestion = questionRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Question not found"));
		questionRepository.delete(existingQuestion);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
	
	@Override
	public List<ServiceAssociatedQuestion> getQuestionByServiceId(String serviceId) throws Exception {
		return questionRepository.findByServiceId(serviceId);
	}

	@Override
	public List<ServiceAssociatedQuestion> getAllQuestion() throws Exception {
		return questionRepository.findAll();
	}

	
}
