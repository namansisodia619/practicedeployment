package com.subhag.services.be.bookingmanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.subhag.services.be.bookingmanagement.repositories.UserRepository;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.User;

@Service(value = "userService")
public class UsersServiceImpl implements UserDetailsService, UsersService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				getAuthority(user));
	}

	private List<SimpleGrantedAuthority> getAuthority(User user) {
		List<RoleName> roles = (List<RoleName>) user.getRoles();
		List<SimpleGrantedAuthority> listToBeReturned = new ArrayList<>();
		for (RoleName roleName : roles) {
			listToBeReturned.add(new SimpleGrantedAuthority(roleName.getRoleName()));
		}
		return listToBeReturned;
	}
}
