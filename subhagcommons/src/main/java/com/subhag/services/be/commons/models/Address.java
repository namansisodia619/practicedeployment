package com.subhag.services.be.commons.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="address")
public class Address  implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  



  public void setId(Long id) {
    this.id = id;
  }

  @Column(name="address_1")
  private String address1;
  
  @Column(name ="address_2")
  private String address2;
  
  
 
 @Column(name="state")
 private String state;
  
  

  @Column(name="country")
  private String country;
  
  @Column(name="city")
  private String city;
  
  @Column(name="pincode")
  private String pincode;
  
   
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }  
 
  public Address() {
    
  }

  public String getCountry() {
    return country;
  }
  


  public void setCountry(String country) {
    this.country = country;
  }
  
 
  
  

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Long getId() {
    return id;
  }


  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

}
