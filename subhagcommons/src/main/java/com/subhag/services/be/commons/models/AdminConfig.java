package com.subhag.services.be.commons.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="admin_config")
public class AdminConfig implements Serializable{
	
	  /**
	   * 
	   */
	  private static final long serialVersionUID = 1L;
	  
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private Long id;
	  
	  private String host;
	  
	  private String userName;
	  
	  private String password;
	  
	  

}
