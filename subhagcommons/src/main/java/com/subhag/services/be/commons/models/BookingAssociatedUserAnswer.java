package com.subhag.services.be.commons.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "booking_associated_user_answer")
public class BookingAssociatedUserAnswer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="userName")
	private String userName;
	
	@Column(name="booking_Id")
	private String bookingId;
	
	@Column(name="answer",columnDefinition = "text")
	private String answer;
	
	public String getuserName() {
		return userName;
	}

	public void setuserName(String userName) {
		this.userName = userName;
	}
	
	public String getbookingId() {
		return bookingId;
	}

	public void setbookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	public String getanswer() {
		return answer;
	}

	public void setanswer(String answer) {
		this.answer = answer;
	}

}
