package com.subhag.services.be.commons.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "booking_associated_comment")
public class BookingComment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="booking_Id")
	private String bookingId;
	
	@Column(name="booking_username")
	private String userName;
	
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name="booking_comment",columnDefinition = "text")
	private String bookingComment;
	
	public String getBookingComment() {
		return bookingComment;
	}

	public void setBookingComment(String bookingComment) {
		this.bookingComment = bookingComment;
	}
	
	public String getbookingId() {
		return bookingId;
	}

	public void setbookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	
}
