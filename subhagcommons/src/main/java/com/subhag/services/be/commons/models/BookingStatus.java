package com.subhag.services.be.commons.models;

public enum BookingStatus {
	NEW,
	ASSIGNED,
	SCHEDULED,
	ONGOING,
	COMPLETE,
	CLOSED
}
