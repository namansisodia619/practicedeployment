package com.subhag.services.be.commons.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "country")
public class Country implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;


  public void setId(Long id) {
    this.id = id;
  }


  @Column(name = "country_Code")
  private String countryCode;

  @Column(name = "country_name")
  private String countryName;



  @OneToMany(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
  @Column(name="states")
  private List<State> state;
  
  public List<State> getState() {
	return state;
}

public void setState(List<State> state) {
	this.state = state;
}

public Country() {
    
  }
  
public Country(String countryCode,String countryName) {
  this.countryCode=countryCode;
  this.countryName=countryName;    
  }


  public Long getId() {
    return id;
  }


  public String getCountryCode() {
    return countryCode;
  }


  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }


  public String getCountryName() {
    return countryName;
  }


  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }


}
