package com.subhag.services.be.commons.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="provider_identity")
public class ProviderIdentity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="registered_employee_id")
	private String registeredEmployeeId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="identity_type")
	private ProviderIdentityEnum identityType;
	
	@Column(name="status")
	private String status; 
	  
	@Column(name="identity_number")
	private String identityNumber;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegisteredEmployeeId() {
		return registeredEmployeeId;
	}

	public void setRegisteredEmployeeId(String registeredEmployeeId) {
		this.registeredEmployeeId = registeredEmployeeId;
	}

	public ProviderIdentityEnum getIdentityType() {
		return identityType;
	}

	public void setIdentityType(ProviderIdentityEnum identityType) {
		this.identityType = identityType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
