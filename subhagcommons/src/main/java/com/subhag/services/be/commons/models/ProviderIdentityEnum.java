package com.subhag.services.be.commons.models;


public enum ProviderIdentityEnum {
    AADHAAR_CARD,
    PASSPORT,
    VOTER_ID,
    PANCARD
}
