package com.subhag.services.be.commons.models;


public enum  RoleNameEum {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_PROVIDER,
    ROLE_PROVIDER_NURSE,
    ROLE_PROVIDER_GYNECOLOGIST,
    ROLE_PROVIDER_CUSTOMER_SERVICE
}
