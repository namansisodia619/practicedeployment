package com.subhag.services.be.commons.models;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "service_associated_question")
public class ServiceAssociatedQuestion {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="service_id")
	private String serviceId;
	
	@Column(name="questions",columnDefinition = "text")
	private String question;
	
	@Column(name="question_type")
	private String questionType;
	
	@Column(name="options",columnDefinition = "text")
	private String options;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getserviceId() {
		return serviceId;
	}

	public void setserviceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getquestion() {
		return question;
	}

	public void setquestion(String question) {
		this.question = question;
	}
	
	public String getquestionType() {
		return questionType;
	}

	public void setquestionType(String questionType) {
		this.questionType = questionType;
	}
	
	public String getoptions() {
		return options;
	}

	public void setoptions(String options) {
		this.options = options;
	}
	
}
