package com.subhag.services.be.commons.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="services")
public class Services implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="service_id",unique=true)
	private String serviceId;	


	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	@Column(name="service_fee")
	private BigDecimal serviceFee;
	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinTable(name = "services_associated_roles", joinColumns = {
			@JoinColumn(name = "services_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "associated_roles_id", referencedColumnName = "id") })
	private List<RoleName> associatedRoles;
	
	public List<RoleName> getAssociatedRoles() {
		return associatedRoles;
	}

	public void setAssociatedRoles(List<RoleName> associatedRoles) {
		this.associatedRoles = associatedRoles;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getServiceFee() {
		return serviceFee;
	}

	public void setServiceFee(BigDecimal serviceFee) {
		this.serviceFee = serviceFee;
	}
}
