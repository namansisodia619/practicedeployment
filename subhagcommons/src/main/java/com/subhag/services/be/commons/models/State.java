package com.subhag.services.be.commons.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;


@Entity
@Table(name="State")
public class State implements Serializable{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

 
  @Column(name = "state_code")
  private String stateCode;

 
  @Column(name = "state_name")
  private String stateName;

  
  public State() {
    
  }
  
  public State(String stateCode, String stateName) {
    this.stateCode=stateCode;
    this.stateName=stateName;
  }

  public Long getId() {
    return id;
  }



  public String getStateCode() {
    return stateCode;
  }

  public void setStateCode(String stateCode) {
    this.stateCode = stateCode;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  } 
  
  }
