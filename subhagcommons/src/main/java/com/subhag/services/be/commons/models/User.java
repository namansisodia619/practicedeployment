package com.subhag.services.be.commons.models;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;


@Entity
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = {"username"})})
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable {

  private static final long serialVersionUID = 1L;
  

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @ApiModelProperty(notes = "The database generated User ID")
  private long id;

  public void setId(long id) {
    this.id = id;
  }



  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }



  public void setUserType(String userType) {
    this.userType = userType;
  }

  @ApiModelProperty(notes = "Users first name")
  @Column(name = "first_name")
  private String firstName;

  @ApiModelProperty(notes = "Users last name")
  @Column(name = "last_name")
  private String lastName;



  @ApiModelProperty(notes = "User creation Date")
  @Column(name = "created_at")
  @JsonIgnore
  @CreatedDate
  private LocalDateTime createdAt;

  @ApiModelProperty(notes = "Users created by")
  @Column(name = "created_by")
  @JsonIgnore
  @CreatedBy
  private String createdBy;

  @ApiModelProperty(notes = "Last modified Date")
  @Column(name = "updated_at")
  @JsonIgnore
  @LastModifiedDate
  private LocalDateTime updatedAt;

  @ApiModelProperty(notes = "Last modification done by")
  @Column(name = "updated_by")
  @JsonIgnore
  @LastModifiedBy
  private String updatedBy;


  @Column(name = "enabled")
  private boolean active;

  @Column(name = "username", nullable = false)
  private String userName;

  @Column(name = "password")
  @JsonIgnore
  private String password;

  @Column(name = "user_dob")
  private Date userDOB;


  @Column(name = "user_gender", nullable = false)
  private String userGender;


  @Column(name = "user_phone_number", nullable = false)
  private String userPhoneNumber;
  
  @Column(name = "mobile_verify")
  private boolean mobileVerify;
  
  @Column(name = "user_email_verify")
  @JsonIgnore
  private String userEmailVerification;
  
  @Column(name = "user_changepassword_verify")
  @JsonIgnore
  private String userChangePassword;

  public String getUserChangePassword() {
	return userChangePassword;
}



public void setUserChangePassword(String userChangePassword) {
	this.userChangePassword = userChangePassword;
}

@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
  @JoinTable(name = "user_roles",
      joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
  private Collection<RoleName> roles;

  @Column(name="address")
  private Address address;

  @Column(name = "user_type")
  private String userType;


  public long getId() {
    return id;
  }

 

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
  @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
  @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
  public LocalDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(LocalDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Date getUserDOB() {
    return userDOB;
  }

  public void setUserDOB(Date userDOB) {
    this.userDOB = userDOB;
  }

  public String getUserGender() {
    return userGender;
  }

  public void setUserGender(GENDER userGender) {
    this.userGender = userGender.toString();
  }

  public String getUserPhoneNumber() {
    return userPhoneNumber;
  }

  public void setUserPhoneNumber(String userPhoneNumber) {
    this.userPhoneNumber = userPhoneNumber;
  }
  
  public boolean ismobileVerify() {
	    return mobileVerify;
  }
  
  public void setmobileVerify(boolean mobileVerify) {
	    this.mobileVerify = mobileVerify;
  }
  
  public String getUserEmailVerification() {
    return userEmailVerification;
  }

  public void setUserEmailVerification(String userEmailVerification) {
    this.userEmailVerification = userEmailVerification;
  }

  public Collection<RoleName> getRoles() {
    return roles;
  }

  public void setRoles(Collection<RoleName> roles) {
    this.roles = roles;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(UserType userType) {
    this.userType = userType.toString();
  }




}
