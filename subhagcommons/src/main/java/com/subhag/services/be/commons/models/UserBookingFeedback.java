package com.subhag.services.be.commons.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "user_booking_feedback")
@EntityListeners(AuditingEntityListener.class)
public class UserBookingFeedback {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="booking_Id")
	private String bookingId;
	
	@Column(name="booking_rating")
	private int rating;
	
	@Column(name="booking_feedback",columnDefinition = "text")
	private String bookingFeedback;
	
	public String getbookingId() {
		return bookingId;
	}

	public void setbookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	public int getrating() {
		return rating;
	}

	public void setrating(int rating) {
		this.rating = rating;
	}
	
	public String getbookingFeedback() {
		return bookingFeedback;
	}

	public void setbookingFeedback(String bookingFeedback) {
		this.bookingFeedback = bookingFeedback;
	}

}
