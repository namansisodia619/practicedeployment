package com.subhag.services.be.commons.models;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "user_spouse")
public class UserSpouse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "The database generated User ID")
	private long id;

	@ApiModelProperty(notes = "Users first name")
	@Column(name = "first_name")
	private String firstName;

	@ApiModelProperty(notes = "Users last name")
	@Column(name = "last_name")
	private String lastName;

	@ApiModelProperty(notes = "User creation Date")
	@Column(name = "created_at")
	@JsonIgnore
	@CreatedDate
	private LocalDateTime createdAt;

	@ApiModelProperty(notes = "Users created by")
	@Column(name = "created_by")
	@JsonIgnore
	@CreatedBy
	private String createdBy;

	@ApiModelProperty(notes = "Last modified Date")
	@Column(name = "updated_at")
	@JsonIgnore
	@LastModifiedDate
	private LocalDateTime updatedAt;

	@ApiModelProperty(notes = "Last modification done by")
	@Column(name = "updated_by")
	@JsonIgnore
	@LastModifiedBy
	private String updatedBy;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "dob")
	private Date DOB;

	@Column(name = "gender", nullable = false)
	private String gender;

	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;

	@Column(name = "address")
	private Address address;

	@OneToOne(fetch = FetchType.LAZY,cascade=CascadeType.REFRESH)
	@JoinColumn(name = "associated_user_id", unique=true)
	@JsonIgnore
	private User associatedUser;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDOB() {
		return DOB;
	}

	public void setDOB(Date dOB) {
		DOB = dOB;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User getAssociatedUser() {
		return associatedUser;
	}

	public void setAssociatedUser(User associatedUser) {
		this.associatedUser = associatedUser;
	}

}
