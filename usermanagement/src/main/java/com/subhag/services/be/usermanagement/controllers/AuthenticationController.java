package com.subhag.services.be.usermanagement.controllers;

import java.util.Map;
import javax.naming.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.usermanagement.config.JwtTokenProvider;
import com.subhag.services.be.usermanagement.domain.ApiResponse;
import com.subhag.services.be.usermanagement.domain.AuthToken;
import com.subhag.services.be.usermanagement.dto.UserForgotPasswordDto;
import com.subhag.services.be.usermanagement.dto.UserLoginDto;
import com.subhag.services.be.usermanagement.dto.UserRegistrationDto;
import com.subhag.services.be.usermanagement.exception.ResourceNotFoundException;
import com.subhag.services.be.usermanagement.services.UsersService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/login")
public class AuthenticationController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenProvider jwtTokenUtil;

  @Autowired
  private UsersService userService;
  
  
/***
 * 
 * @param loginUser
 * @return
 * @throws AuthenticationException
 * @throws ResourceNotFoundException
 */
  @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
  public ApiResponse<AuthToken> register(@RequestBody UserLoginDto loginUser)
      throws AuthenticationException, ResourceNotFoundException {

    Authentication authentication = authenticationManager.authenticate(
        (Authentication) new UsernamePasswordAuthenticationToken(loginUser.getUserName(),
            loginUser.getPassword()));
    final User users = userService.findByUserName(loginUser.getUserName());
   
    if(!users.isActive())
      return new ApiResponse<AuthToken>(500,"User not Active, Please activate the user by  clicking the link sent to your registered e-Mail.",new AuthToken("",loginUser.getUserName()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    final String token = jwtTokenUtil.generateToken(authentication, loginUser.getUserName());
    return new ApiResponse<AuthToken>(200, "success", new AuthToken(token, users.getUserName()));
  }
  
  /***
   * 
   * @param userRegistrationDto
   * @return
   * @throws Exception
   */
  
  @RequestMapping(value = "/signup", method = RequestMethod.POST)
  public ApiResponse<UserRegistrationDto> signup(
      @RequestBody UserRegistrationDto userRegistrationDto) throws Exception {
    return new ApiResponse<UserRegistrationDto>(200, "success", userService.userRegistration(userRegistrationDto));
  }
  
  /***
   * 
   * @param verificationStrin
   * @return
   * @throws Exception
   */
  
  
  @RequestMapping(value = "/verify/{id}", method = RequestMethod.GET, produces="application/json")
  public  String verify(@PathVariable(value = "id") String verificationString ) throws Exception {
    return userService.verifyEmail(verificationString);
  }
  
  @RequestMapping(value = "/forgotpwd", method = RequestMethod.POST, produces="application/json")
  public  Map<String, Boolean> forgotPassword(@RequestBody UserForgotPasswordDto userForgotPasswordDto) throws Exception {
    return userService.forgotPassword(userForgotPasswordDto);
  }
  
  @RequestMapping(value = "/changepassword", method = RequestMethod.POST, produces="application/json")
  public  Map<String, Boolean> changePassword(@RequestBody UserForgotPasswordDto userForgotPasswordDto) throws Exception {
    return userService.changePassword(userForgotPasswordDto);
  }

}
