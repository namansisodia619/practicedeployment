package com.subhag.services.be.usermanagement.controllers;

import java.util.List;



import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.subhag.services.be.commons.models.Country;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.commons.models.UserSpouse;
import com.subhag.services.be.usermanagement.dto.ProviderIdentityDto;
import com.subhag.services.be.usermanagement.dto.UserNameDto;
import com.subhag.services.be.usermanagement.dto.UserOTPMobileVerificationDto;
import com.subhag.services.be.usermanagement.dto.UserRegistrationDto;
import com.subhag.services.be.usermanagement.dto.UserSpouseDto;
import com.subhag.services.be.usermanagement.exception.ResourceNotFoundException;
import com.subhag.services.be.usermanagement.services.UsersService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/users")
@Api(value = "usermanagement", description = "Operation Pretaining to Subhag User Management")
public class UserController {

	@Autowired
	private UsersService usersService;

	/**
	 * Get all users list.
	 *
	 * @return the list
	 */
	@ApiOperation(value = "View a list of available users", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved users list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public List<User> getAllUsers() {
		return usersService.getAllUsers();
	}

	/**
	 * Update user response entity.
	 *
	 * @param userId
	 *            the user id
	 * @param userDetails
	 *            the user details
	 * @return the response entity
	 * @throws ResourceNotFoundException
	 *             the resource not found exception
	 */
	@ApiOperation(value = "Update a user")
	@RequestMapping(value = "/updateuser", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<User> updateUser(	@Valid @RequestBody User userDetails) throws ResourceNotFoundException {
		return ResponseEntity.ok(usersService.updateUser(userDetails));
	}

	/**
	 * Delete user map.
	 *
	
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	@ApiOperation(value = "Delete a user")
	@RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long id) throws Exception {
		return usersService.deleteUser(id);
	}

	@ApiOperation(value = "Get a user")
	@RequestMapping(value = "/user", method = RequestMethod.POST, produces = "application/json")
	public User getUser(@RequestBody UserNameDto userName) throws Exception {
		return usersService.findByUserName(userName.getUserName());
	}

	@ApiOperation(value = "Get All Country and its States")
	@RequestMapping(value = "/user/countries", method = RequestMethod.GET, produces = "application/json")
	public List<Country> getCountries() throws Exception {
		return usersService.getAllCountries();
	}

	@ApiOperation(value = "Register Provider")
	@RequestMapping(value = "/user/provider", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public User registerProvider(@RequestBody UserRegistrationDto userRegistrationDto) throws Exception {
		return usersService.providerRegistartion(userRegistrationDto);
	}
	
	@ApiOperation(value = "Register Manager")
	@RequestMapping(value = "/user/manager", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public User registerManager(@RequestBody UserRegistrationDto userRegistrationDto) throws Exception {
		return usersService.managerRegistration(userRegistrationDto);
	}
	
	@ApiOperation(value = "Available Roles")
	@RequestMapping(value = "/roles", method = RequestMethod.GET, produces = "application/json")
	public List<RoleName> getRoles() {
		return usersService.getRoles();
	}
	
	
	@ApiOperation(value = "Add Spouse details")
	@RequestMapping(value = "/addspouse", method = RequestMethod.POST, produces = "application/json")
	public UserSpouse updateSpouse(@RequestBody UserSpouseDto userSpouseDto) throws Exception {
	return usersService.addSpouseDetails(userSpouseDto);
	}
	
	@ApiOperation(value = "Get Spouse details")
	@RequestMapping(value = "/getspouse", method = RequestMethod.POST, produces = "application/json")
	public UserSpouse getSpouseDetails(@RequestBody UserNameDto userName) throws Exception {
	return usersService.getSpouseDetails(userName.getUserName());
	}
	
	@ApiOperation(value = "Update Spouse details")
	@RequestMapping(value = "/updatespouse", method = RequestMethod.PUT, produces = "application/json")
	public UserSpouse updateSpousedetails(@RequestBody UserSpouse userSpouse) throws Exception {
	return usersService.updateSpouseDetails(userSpouse);
	}
	
	/**
	 * Get all users list by UserType.
	 *
	 * @return the list
	 */
	@ApiOperation(value = "View a list of available users By UserType", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved users list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@RequestMapping(value = "/users/{UserType}", method = RequestMethod.GET, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public List<User> getAllUsersByUserType(@PathVariable(value = "UserType") String UserType) {
		return usersService.findByUserType(UserType);
	}
	

	// send otp to mobile number 	
	@ApiOperation(value = "send otp to mobile number")
	@RequestMapping(value = "/mobilenumber/{mobilenumber}/sendOtp",method = RequestMethod.GET, produces = "application/json")
	public Map sendOTP(@PathVariable("mobilenumber") String mobileNumber) throws Exception{
		return usersService.sendOtp(mobileNumber);		
	}
	
	// verify mobile number 	
	@ApiOperation(value = "verify mobile number")
	@RequestMapping(value = "/mobilenumber/verifyOtp",method = RequestMethod.POST, produces = "application/json")
	public Map verifyOTP(@RequestBody UserOTPMobileVerificationDto otpBody) throws Exception {
		return usersService.verifyOtp(otpBody);
	}
	
	@ApiOperation(value = "Add Provider Identity")
	@RequestMapping(value = "/add-provider-identity", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Map<String, String> addProviderIdentity(@RequestBody ProviderIdentityDto providerIdentityDto) throws Exception {
		return usersService.addProviderIdentity(providerIdentityDto);
	}
	@ApiOperation(value = "Update Provider Identity")
	@RequestMapping(value = "/update-provider-identity", method = RequestMethod.PUT, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Map<String, String> updateProviderIdentity(@RequestBody ProviderIdentityDto providerIdentityDto) throws Exception {
		return usersService.updateProviderIdentity(providerIdentityDto);
	}
	
	@ApiOperation(value = "Get Provider Identity")
	@RequestMapping(value = "/get-provider-identity", method = RequestMethod.POST, produces = "application/json")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public ProviderIdentityDto getProviderIdentity(@RequestBody UserNameDto userName) throws Exception {
		return usersService.getProviderIdentity(userName.getUserName());
	}

}
