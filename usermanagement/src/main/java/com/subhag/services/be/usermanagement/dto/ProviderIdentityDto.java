package com.subhag.services.be.usermanagement.dto;

import com.subhag.services.be.commons.models.ProviderIdentityEnum;

public class ProviderIdentityDto {
	
	private Long id;
	
	private String registeredEmployeeId;
	
	private ProviderIdentityEnum identityType;
	
	private String identityNumber;
	
	private String status;
	
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegisteredEmployeeId() {
		return registeredEmployeeId;
	}

	public void setRegisteredEmployeeId(String registeredEmployeeId) {
		this.registeredEmployeeId = registeredEmployeeId;
	}

	public ProviderIdentityEnum getIdentityType() {
		return identityType;
	}

	public void setIdentityType(ProviderIdentityEnum identityType) {
		this.identityType = identityType;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
