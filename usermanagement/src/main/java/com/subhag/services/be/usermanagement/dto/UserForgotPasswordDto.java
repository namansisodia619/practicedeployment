package com.subhag.services.be.usermanagement.dto;

public class UserForgotPasswordDto {
	private String userName;
	private String password;
	private String verificationString;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerificationString() {
		return verificationString;
	}
	public void setVerificationString(String verificationString) {
		this.verificationString = verificationString;
	}
	

}
