package com.subhag.services.be.usermanagement.dto;

public class UserNameDto {
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
