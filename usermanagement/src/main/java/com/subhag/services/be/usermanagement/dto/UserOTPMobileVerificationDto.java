package com.subhag.services.be.usermanagement.dto;

public class UserOTPMobileVerificationDto {
	
	private String otp;
	private String mobileNumber;
	private String userName;
	
	
	public String getotp() {
		return otp;
	}

	public void setotp(String otp) {
		this.otp = otp;
	}
	
	public String getmobileNumber() {
		return mobileNumber;
	}

	public void setmobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}
