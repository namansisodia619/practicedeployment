package com.subhag.services.be.usermanagement.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import com.subhag.services.be.commons.models.GENDER;
import com.subhag.services.be.commons.models.RoleName;

public class UserRegistrationDto implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String firstName;

  private String lastName;

  private String userName;

  private String password;

  private String phoneNumber;

  private GENDER gender;

  private Date dob;

  private String address1;

  private String address2;

  private String country;

  private String city;

  private String pincode;

  private String state;
  
  private List<String> roleName;
  
  private String userType;

  private ProviderIdentityDto identity;

public String getUserType() {
	return userType;
}

public void setUserType(String userType) {
	this.userType = userType;
}

public List<String> getRoleName() {
	return roleName;
}

public void setRoleName(List<String> roleName) {
	this.roleName = roleName;
}

public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public GENDER getGender() {
    return gender;
  }

  public void setGender(GENDER gender) {
    this.gender = gender;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public ProviderIdentityDto getIdentity() {
	return identity;
  }

  public void setIdentity(ProviderIdentityDto identity) {
	this.identity = identity;
  }

}
