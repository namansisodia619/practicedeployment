package com.subhag.services.be.usermanagement.repositories;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.subhag.services.be.commons.models.Address;
import com.subhag.services.be.commons.models.User;

@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")

public interface AddressRepository  extends JpaRepository<Address, Long> {

}
