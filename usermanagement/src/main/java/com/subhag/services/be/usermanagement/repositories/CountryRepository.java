package com.subhag.services.be.usermanagement.repositories;

import java.util.List;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.subhag.services.be.commons.models.Country;
import com.subhag.services.be.commons.models.State;

@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")

public interface CountryRepository extends JpaRepository<Country, Long> {
  
  Country findByCountryCode(String countrCode);
  
  List<State> findAllByCountryCode(String countryCode);
  
  

}
