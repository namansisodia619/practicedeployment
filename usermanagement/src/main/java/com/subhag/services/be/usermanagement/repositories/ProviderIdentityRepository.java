package com.subhag.services.be.usermanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.subhag.services.be.commons.models.ProviderIdentity;
import com.subhag.services.be.commons.models.User;

public interface ProviderIdentityRepository extends JpaRepository<ProviderIdentity, Long> {
	
	ProviderIdentity findByUser(User user);
}
