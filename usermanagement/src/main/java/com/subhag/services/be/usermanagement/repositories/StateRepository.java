package com.subhag.services.be.usermanagement.repositories;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.subhag.services.be.commons.models.Country;
import com.subhag.services.be.commons.models.State;

@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")

public interface StateRepository extends JpaRepository<State, Long> {
  State findByStateCode(String stateCode);

}
