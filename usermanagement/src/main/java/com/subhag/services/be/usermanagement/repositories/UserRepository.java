package com.subhag.services.be.usermanagement.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.subhag.services.be.commons.models.User;


@Repository
@Transactional
@EntityScan("com.subhag.services.be.commons.models")

/***
 * 
 * @author Rajesh V
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

  User findByUserName(String userName);
  
  User findByUserEmailVerification(String VerificationString);
  
  User findByUserChangePassword(String VerificationString);


  @Query("SELECT u FROM User u WHERE u.userType LIKE CONCAT(:usertype,'%')")
  List<User> findByUserType(@Param("usertype") String userType);
  
 // List<User> findByUserType(String userType);
}

