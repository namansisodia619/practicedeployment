package com.subhag.services.be.usermanagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.commons.models.UserSpouse;

public interface UserSpouseRepository extends JpaRepository<UserSpouse, Long> {
	UserSpouse findByAssociatedUser(User user);
	
	

}
