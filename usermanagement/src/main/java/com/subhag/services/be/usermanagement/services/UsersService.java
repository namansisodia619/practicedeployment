package com.subhag.services.be.usermanagement.services;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.subhag.services.be.commons.models.Country;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.commons.models.UserSpouse;
import com.subhag.services.be.usermanagement.dto.ProviderIdentityDto;
import com.subhag.services.be.usermanagement.dto.UserForgotPasswordDto;
import com.subhag.services.be.usermanagement.dto.UserNameDto;
import com.subhag.services.be.usermanagement.dto.UserOTPMobileVerificationDto;
import com.subhag.services.be.usermanagement.dto.UserRegistrationDto;
import com.subhag.services.be.usermanagement.dto.UserSpouseDto;
import com.subhag.services.be.usermanagement.exception.ResourceNotFoundException;

public interface UsersService {
	public List<User> getAllUsers();

	public User userRegistration(UserRegistrationDto user) throws Exception;

	public User updateUser(User user) throws ResourceNotFoundException;

	public Map<String, Boolean> deleteUser(Long id) throws Exception;

	public String verifyEmail(String verificationString) throws ResourceNotFoundException;
	
	public Map<String, Boolean> forgotPassword(UserForgotPasswordDto userForgotPasswordDto) throws ResourceNotFoundException;
	
	public Map<String, Boolean> changePassword(UserForgotPasswordDto userForgotPasswordDto) throws ResourceNotFoundException;

	User findByUserName(String userName) throws ResourceNotFoundException;

	List<Country> getAllCountries();

	User providerRegistartion(UserRegistrationDto userRegistrationDto) throws Exception;

	User managerRegistration(UserRegistrationDto userRegistrationDto) throws Exception;

	List<RoleName> getRoles();

	UserSpouse addSpouseDetails(UserSpouseDto userSpouseDto) throws Exception;

	UserSpouse getSpouseDetails(String userName) throws Exception;

	UserSpouse updateSpouseDetails(UserSpouse userSpouse) throws Exception;
	
	List<User> findByUserType(String UserType);
	
	Map sendOtp(String mobileNumber) throws ResourceNotFoundException;
	
	Map verifyOtp(UserOTPMobileVerificationDto otpBody) throws Exception;
	
	Map<String, String> addProviderIdentity(ProviderIdentityDto providerIdentityDto) throws Exception;
	Map<String, String> updateProviderIdentity(ProviderIdentityDto providerIdentityDto) throws Exception;
	ProviderIdentityDto getProviderIdentity(String userName) throws Exception;

}
