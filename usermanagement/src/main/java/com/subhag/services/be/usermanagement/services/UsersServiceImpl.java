package com.subhag.services.be.usermanagement.services;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.common.hash.Hashing;
import com.subhag.services.be.commons.models.Address;
import com.subhag.services.be.commons.models.Country;
import com.subhag.services.be.commons.models.Mail;
import com.subhag.services.be.commons.models.ProviderIdentity;
import com.subhag.services.be.commons.models.RoleName;
import com.subhag.services.be.commons.models.User;
import com.subhag.services.be.commons.models.UserSpouse;
import com.subhag.services.be.commons.models.UserType;
import com.subhag.services.be.usermanagement.config.EmailService;
import com.subhag.services.be.usermanagement.config.MSG91MessageConfig;
import com.subhag.services.be.usermanagement.dto.ProviderIdentityDto;
import com.subhag.services.be.usermanagement.dto.UserForgotPasswordDto;
import com.subhag.services.be.usermanagement.dto.UserNameDto;
import com.subhag.services.be.usermanagement.dto.UserOTPMobileVerificationDto;
import com.subhag.services.be.usermanagement.dto.UserRegistrationDto;
import com.subhag.services.be.usermanagement.dto.UserSpouseDto;
import com.subhag.services.be.usermanagement.exception.ResourceNotFoundException;
import com.subhag.services.be.usermanagement.repositories.AddressRepository;
import com.subhag.services.be.usermanagement.repositories.CountryRepository;
import com.subhag.services.be.usermanagement.repositories.ProviderIdentityRepository;
import com.subhag.services.be.usermanagement.repositories.RoleNameRepository;
import com.subhag.services.be.usermanagement.repositories.UserRepository;
import com.subhag.services.be.usermanagement.repositories.UserSpouseRepository;


@Service(value = "userService")
public class UsersServiceImpl implements UserDetailsService, UsersService {
	
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	@Autowired
	private EmailService emailService;
	
	
	private MSG91MessageConfig messageService;

	@Autowired
	private RoleNameRepository roleNameRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private RoleNameRepository roleRepository;

	@Autowired
	private UserSpouseRepository userSpouseRepository;
	
	@Autowired
	private ProviderIdentityRepository providerIdentityRepository;
	

	@Value("${server.prefix}")
	private String serverPrefix;
	
	@Value("${server.forgotpassword}")
	private String forgotpwdLink;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
				getAuthority(user));

	}

	private List<SimpleGrantedAuthority> getAuthority(User user) {

		List<RoleName> roles = (List<RoleName>) user.getRoles();
		List<SimpleGrantedAuthority> listToBeReturned = new ArrayList<>();
		for (RoleName roleName : roles) {
			listToBeReturned.add(new SimpleGrantedAuthority(roleName.getRoleName()));
		}

		return listToBeReturned;
	}

	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	public User updateUser(User user) throws ResourceNotFoundException {
		User existingUser = userRepository.findByUserName(user.getUserName());
		if (existingUser == null)
			throw new ResourceNotFoundException("User not found on :: " + user.getUserName());

		user.setId(existingUser.getId());
		user.setPassword(existingUser.getPassword());

		final User updatedUser = userRepository.save(user);
		return updatedUser;
	}

	public Map<String, Boolean> deleteUser(Long id) throws Exception {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User is not present in the"));
		userRepository.delete(user);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	public User findByUserName(String userName) throws ResourceNotFoundException {
		User user = userRepository.findByUserName(userName);

		if (user == null)
			throw new ResourceNotFoundException("User Not found :" + userName);

		return user;
	}

	@Override
	public User userRegistration(UserRegistrationDto userRegistrationDto) throws Exception {
		return populateUserForRegistrion(userRegistrationDto, userRegistrationDto.getUserType());
	}

	public User populateUserForRegistrion(UserRegistrationDto userRegistrationDto, String userType) throws Exception {
		User user = new User();
		user.setActive(false);

		user.setFirstName(userRegistrationDto.getFirstName());
		user.setLastName(userRegistrationDto.getLastName());
		if (userType.contains("USER")) {
			user.setRoles(Arrays.asList(roleNameRepository.findByRoleName(userType)));
			user.setPassword(bcryptEncoder.encode(userRegistrationDto.getPassword()));
			user.setUserType(UserType.USER);
		} else if (userType.contains("PROVIDER") || userType.contains("MANAGER")) {
			user.setRoles(Arrays.asList(roleNameRepository.findByRoleName(userType)));
			user.setPassword(bcryptEncoder.encode(userRegistrationDto.getUserName()));
			user.setUserType(userType);
		} else {
			throw new Exception("This user type " + userType + " creation is not allowed");
		}

		user.setAddress(returnAddress(userRegistrationDto));
		user.setUserDOB(userRegistrationDto.getDob());
		String verificationKey = Hashing.sha256()
				.hashString(userRegistrationDto.getUserName() + ":" + userRegistrationDto.getPassword(),
						StandardCharsets.UTF_8)
				.toString();
		user.setUserEmailVerification(verificationKey);
		user.setUserGender(userRegistrationDto.getGender());
		user.setUserName(userRegistrationDto.getUserName());
		user.setUserPhoneNumber(userRegistrationDto.getPhoneNumber());

		Mail mail = new Mail();
		mail.setFrom("noreply@subhag.in");
		mail.setTo(new String[] { userRegistrationDto.getUserName() });
		mail.setSubject("Email verification for Subhag Application");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("verificationlink", serverPrefix + "/login/verify/" + verificationKey);
		model.put("name", userRegistrationDto.getFirstName());
		model.put("location", "Bangalore");
		model.put("signature", "http://subhag.in");
		mail.setModel(model);

		User userToBereturned = userRepository.save(user);
		if (userToBereturned != null) {

			try {
				emailService.sendSimpleMessage(mail);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return userToBereturned;
	}

	@Override
	public String verifyEmail(String verificationString) throws ResourceNotFoundException {

		User user = userRepository.findByUserEmailVerification(verificationString);
		if (user == null)
			throw new ResourceNotFoundException("User not found");
		user.setActive(true);
		user.setUserEmailVerification("");
		userRepository.save(user);
		return "Welcome to subhag your account is activated";

	}

	@Override
	public List<Country> getAllCountries() {

		return countryRepository.findAll();
	}

	public Address returnAddress(UserRegistrationDto userRegistrionDto) {
		/*
		 * State state=stateRepository.findByStateCode(userRegistrionDto.getState());
		 * Country
		 * country=countryRepository.findByCountryCode(userRegistrionDto.getCountry());
		 */
		Address address = new Address();
		address.setAddress1(userRegistrionDto.getAddress1());
		address.setAddress2(userRegistrionDto.getAddress2());
		address.setCity(userRegistrionDto.getCity());
		address.setPincode(userRegistrionDto.getPincode());
		address.setState(userRegistrionDto.getState());
		address.setCountry(userRegistrionDto.getCountry());
		return addressRepository.save(address);
	}

	@Override
	public User providerRegistartion(UserRegistrationDto userRegistrationDto) throws Exception {
		// TODO Auto-generated method stub

		return populateUserForRegistrion(userRegistrationDto, userRegistrationDto.getUserType());
	}

	public List<RoleName> getRoles() {
		// TODO Auto-generated method stub
		return roleRepository.findAll();
	}

	@Override
	public User managerRegistration(UserRegistrationDto userRegistrationDto) throws Exception {
		// TODO Auto-generated method stub
		return populateUserForRegistrion(userRegistrationDto, userRegistrationDto.getUserType());
	}

	@Override
	public UserSpouse addSpouseDetails(UserSpouseDto userSpouseDto) throws Exception {
		User associatedUser = userRepository.findByUserName(userSpouseDto.getAssociatedUser());
		if (associatedUser == null) {
			throw new Exception("No associated user present");
		}
		UserSpouse userSpouse = new UserSpouse();
		userSpouse.setFirstName(userSpouseDto.getFirstName());
		userSpouse.setLastName(userSpouseDto.getLastName());
		userSpouse.setCreatedAt(LocalDateTime.now());
		userSpouse.setCreatedBy(userSpouseDto.getAssociatedUser());
		userSpouse.setEmail(userSpouseDto.getEmail());
		userSpouse.setDOB(userSpouseDto.getDob());
		userSpouse.setPhoneNumber(userSpouseDto.getPhoneNumber());
		userSpouse.setGender(userSpouseDto.getGender().toString());
		userSpouse.setAddress(returnSpouseAddress(userSpouseDto));
		userSpouse.setAssociatedUser(associatedUser);

		return userSpouseRepository.save(userSpouse);
	}

	public Address returnSpouseAddress(UserSpouseDto userSpouseDto) {
		/*
		 * State state=stateRepository.findByStateCode(userRegistrionDto.getState());
		 * Country
		 * country=countryRepository.findByCountryCode(userRegistrionDto.getCountry());
		 */
		Address address = new Address();
		address.setAddress1(userSpouseDto.getAddress1());
		address.setAddress2(userSpouseDto.getAddress2());
		address.setCity(userSpouseDto.getCity());
		address.setPincode(userSpouseDto.getPincode());
		address.setState(userSpouseDto.getState());
		address.setCountry(userSpouseDto.getCountry());
		return addressRepository.save(address);
	}

	@Override
	public UserSpouse getSpouseDetails(String userName) throws Exception {
		// TODO Auto-generated method stub
		User user=userRepository.findByUserName(userName);
		if(user==null) {
			throw new Exception("User is not present");
		}
		UserSpouse userSpouse = userSpouseRepository.findByAssociatedUser(user);
		if (userSpouse == null) {
			throw new Exception("Spouse details not found");
		}
		return userSpouse;
	}

	@Override
	public UserSpouse updateSpouseDetails(UserSpouse userSpouse) throws Exception {
		// TODO Auto-generated method stub
		UserSpouse existingUserSpouse = userSpouseRepository.findById(userSpouse.getId()).orElseThrow(() -> new ResourceNotFoundException("Spouse details not available"));
		userSpouse.setAssociatedUser(existingUserSpouse.getAssociatedUser());
		userSpouse.setId(existingUserSpouse.getId());

		return userSpouseRepository.save(userSpouse);
		
	}

	@Override
	public Map<String, Boolean> forgotPassword(UserForgotPasswordDto userForgotPasswordDto) throws ResourceNotFoundException {
		User user = userRepository.findByUserName(userForgotPasswordDto.getUserName());
		if (user == null)
			throw new ResourceNotFoundException("User not found");
		String verificationKey = Hashing.sha256()
				.hashString(user.getUserName() + ":" + user.getPassword(),
						StandardCharsets.UTF_8)
				.toString();
		user.setUserChangePassword(verificationKey);
		
		
		Mail mail = new Mail();
		mail.setFrom("noreply@subhag.in");
		mail.setTo(new String[] { user.getUserName() });
		mail.setSubject("Email verification for Subhag Application");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("verificationlink", forgotpwdLink + "?email="+user.getUserName()+"&key=" + verificationKey);
		model.put("name", user.getFirstName());
		model.put("location", "Bangalore");
		model.put("signature", "http://subhag.in");
		mail.setModel(model);

		User userToBereturned = userRepository.save(user);
		if (userToBereturned != null) {

			try {
				emailService.sendForgotPasswordVerificationEmail(mail);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("User " + user.getUserName() + "  Activated ", Boolean.TRUE);
		return response;
	}

	@Override
	public Map<String, Boolean> changePassword(UserForgotPasswordDto userForgotPasswordDto)
			throws ResourceNotFoundException {
		User user=userRepository.findByUserChangePassword(userForgotPasswordDto.getVerificationString());
		if(user==null) {
			throw new ResourceNotFoundException("Verification String submitted not present and you are not authorized to change the password for "+userForgotPasswordDto.getUserName());
		}
		
		user.setPassword(bcryptEncoder.encode(userForgotPasswordDto.getPassword()));
		user.setUserChangePassword("");
		userRepository.save(user);
		Map<String, Boolean> response = new java.util.HashMap<>();
		response.put("User " + user.getUserName() + "  Activated ", Boolean.TRUE);
		return response;	
		
	}
	
	@Override
	public List<User> findByUserType(String UserType) {
		// TODO Auto-generated method stub
		return userRepository.findByUserType(UserType);
	}



	@Override
	public Map verifyOtp(UserOTPMobileVerificationDto otpBody) throws Exception {
		
        //Prepare Url
        URLConnection myURLConnection=null;
        URL myURL=null;
        BufferedReader reader=null;

        //Verify otp api
        String mainUrl="https://control.msg91.com/api/verifyRequestOTP.php?";

        //Prepare parameter string
        StringBuilder sbPostData= new StringBuilder(mainUrl);
        sbPostData.append("authkey="+messageService.AUTH_KEY);
        sbPostData.append("&mobile="+otpBody.getmobileNumber());
        sbPostData.append("&otp="+otpBody.getotp());

        //final string
        mainUrl = sbPostData.toString();
        try
        {
            //prepare connection
            myURL = new URL(mainUrl);
            myURLConnection = myURL.openConnection();
            myURLConnection.connect();
            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            //reading response
            String response;
            while ((response = reader.readLine()) != null) {
            	 if(response.contains("success") || response.contains("already_verified")) {
            		 User user = userRepository.findByUserName(otpBody.getUserName());
            		 if (user == null)
						throw new ResourceNotFoundException("User not found");
            		 user.setmobileVerify(true);
            		 userRepository.save(user);
                     return Collections.singletonMap("message", "otp is verified successfully");
            	 }
                 else {
                 	throw new ResourceNotFoundException(response); 
                 }
            }

            //finally close connection
            reader.close();
            throw new ResourceNotFoundException("Something Unexpected Occured"); 
        }
        catch (IOException e)
        {
                e.printStackTrace();
                throw new ResourceNotFoundException("Something Unexpected Occured");
        }
        
	}

	@Override
	public Map sendOtp(String mobileNumber) throws ResourceNotFoundException {
		
		 //Prepare Url
        URLConnection myURLConnection=null;
        URL myURL=null;
        BufferedReader reader=null;

        //encoding message
        String encoded_message=URLEncoder.encode(messageService.OTP_MESSAGE_TEMPLATE);

        //Send SMS API
        String mainUrl="http://control.msg91.com/api/sendotp.php?";
        
        //Prepare parameter string
        StringBuilder sbPostData= new StringBuilder(mainUrl);
        sbPostData.append("authkey="+messageService.AUTH_KEY);
        sbPostData.append("&mobiles="+mobileNumber);
        sbPostData.append("&message="+encoded_message);
        sbPostData.append("&sender="+messageService.SENDER_ID);
        
        mainUrl = sbPostData.toString();
        try
        {
            //prepare connection
            myURL = new URL(mainUrl);
            myURLConnection = myURL.openConnection();
            myURLConnection.connect();
            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            //reading response
            String response;
            while ((response = reader.readLine()) != null)  {
            if(response.contains("success"))
            	return Collections.singletonMap("message", "otp is send successfully");
            else {
            	throw new ResourceNotFoundException(response); 
            }
            }
            //finally close connection
            reader.close();
            throw new ResourceNotFoundException("Something Unexpected Occured"); 
        }
        catch (IOException e)
        {
                e.printStackTrace();
                throw new ResourceNotFoundException("Something Unexpected Occured");
        }
        
	}
	
	@Override
	public Map<String, String> addProviderIdentity(ProviderIdentityDto providerIdentityDto) throws Exception {
		User user=userRepository.findByUserName(providerIdentityDto.getUserName());
		if(user==null) {
			throw new Exception("User not present");
		}
		ProviderIdentity providerIdentity = new ProviderIdentity();
		
		providerIdentity.setIdentityNumber(providerIdentityDto.getIdentityNumber());
		providerIdentity.setIdentityType(providerIdentityDto.getIdentityType());
		providerIdentity.setRegisteredEmployeeId(providerIdentityDto.getRegisteredEmployeeId());
		providerIdentity.setStatus(providerIdentityDto.getStatus());
		providerIdentity.setUser(user);
		providerIdentityRepository.save(providerIdentity);
		
		Map<String, String> response = new HashMap<>();
		response.put("message", "Provider identity added successfully");
		return response;
	}
	
	@Override
	public Map<String, String> updateProviderIdentity(ProviderIdentityDto providerIdentityDto) throws Exception {
		ProviderIdentity providerIdentity = providerIdentityRepository.findById(providerIdentityDto.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Provider Identity not found"));
				
		providerIdentity.setIdentityNumber(providerIdentityDto.getIdentityNumber());
		providerIdentity.setIdentityType(providerIdentityDto.getIdentityType());
		providerIdentity.setRegisteredEmployeeId(providerIdentityDto.getRegisteredEmployeeId());
		providerIdentity.setStatus(providerIdentityDto.getStatus());
		
		providerIdentityRepository.save(providerIdentity);
		
		Map<String, String> response = new HashMap<>();
		response.put("message", "Provider identity update successfully");
		return response;
	}

	@Override
	public ProviderIdentityDto getProviderIdentity(String userName) throws Exception {
		User user = userRepository.findByUserName(userName);
		if(user==null)		
				throw  new ResourceNotFoundException("User is not present");
		
		ProviderIdentity providerIdentity = providerIdentityRepository.findByUser(user);
		
		if(providerIdentity == null) {
			throw new Exception("provider identity not found");
		}
		
		ProviderIdentityDto providerIdentityDto = new ProviderIdentityDto();
		providerIdentityDto.setId(providerIdentity.getId());
		providerIdentityDto.setIdentityNumber(providerIdentity.getIdentityNumber());
		providerIdentityDto.setIdentityType(providerIdentity.getIdentityType());
		providerIdentityDto.setRegisteredEmployeeId(providerIdentity.getRegisteredEmployeeId());
		providerIdentityDto.setStatus(providerIdentity.getStatus());		
		return providerIdentityDto;
	}


}
